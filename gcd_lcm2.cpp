#include <bits/stdc++.h>

using namespace std;

int HCF(int a, int b) {
	int divisor = b, divident = a, r = a % b;
	while (r != 0) {
		divident = divisor;
		divisor = r;
		r = divident % divisor;
	}
	return divisor;
}

int LCM(int a, int b) {
	int pro, hcf;
	hcf = HCF(a, b);
	pro = a * b;
	return pro / hcf;
}


int getTotalX(vector <int> a, vector <int> b) {
	// Complete this function
	int sizea = a.size(), sizeb = b.size();
	int numa = LCM(a[0], a[1]), numb = HCF(b[0], b[1]);
	while (sizea != 0) {
		numa = LCM(numa, a[sizea - 1]);
		sizea--;
	}
	while (sizeb != 0) {
		numb = HCF(numb, b[sizeb - 1]);
		sizeb--;
	}
	//cout<<numa<<" "<<numb<<endl;
	int count = 0, itra = 1;
	while (itra*numa <= numb) {
		if (numb % (numa*itra) == 0) {
			count++;
		}
		itra++;
	}
	return count;

}

int main() {
	int n;
	int m;
	cin >> n >> m;
	vector<int> a(n);
	for (int a_i = 0; a_i < n; a_i++) {
		cin >> a[a_i];
	}
	vector<int> b(m);
	for (int b_i = 0; b_i < m; b_i++) {
		cin >> b[b_i];
	}
	int total = getTotalX(a, b);
	cout << total << endl;

	return 0;
}