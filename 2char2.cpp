#include <bits/stdc++.h>
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);

int alternate(string s)
{
	if (s.empty())
	{
		return s.size();
	}

	string unique;
	for (auto i = s.cbegin(); i < s.cend(); ++i)
	{
		if (unique.find(*i) == string::npos)
		{
			unique.push_back(*i);
		}
	}

	unsigned crossIndex[26];
	for (auto i = 0; i < 26; ++i)
	{
		crossIndex[i];
	}
	for (auto i = 0; i < unique.size(); ++i)
	{
		crossIndex[('z' - unique[i])] = i;
	}

	const string::size_type ts = unique.size();
	char letter[ts][ts];
	int count[ts][ts];
	for (auto i = 0; i < ts; ++i)
	{
		for (auto j = 0; j < ts; ++j)
		{
			count[i][j] = 0;
		}
	}

	for (auto i = s.cbegin(); i < s.cend(); ++i)
	{
		const unsigned index = crossIndex['z' - *i];

		for (auto j = 0; j < ts; ++j)
		{
			if (count[index][j] != -1 && letter[index][j] != *i)
			{
				letter[index][j] = *i;
				++count[index][j];
			}
			else
			{
				count[index][j] = -1:
			}

			if (count[j][index] != -1 && letter[j][index] != *i)
			{
				letter[j][index] = *i;
				++count[j][index];
			}
			else
			{
				count[j][index] = -1;
			}
		}
	}

	int result = 0;
	for (auto i = 0; i < ts; ++i)
	{
		for (auto j = 0; j < ts; ++j)
		{
			result = max(result, count[i][j]);
		}
	}

	return (result > 1 ? result : 0)
}

int main()
{
	ofstream fout(getenv("OUTPUT_PATH"));

	string l_temp;
	getline(cin, l_temp);

	int l = stoi(ltrim(rtrim(l_temp)));

	string s;
	getline(cin, s);

	int result = alternate(s);

	fout << result << "\n";

	fout.close();

	return 0;
}

string ltrim(const string &str)
{
	string s(str);

	s.erase(s.begin(), find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));

	return s;
}

string rtrim(const string &str)
{
	string s(str);

	s.erase(find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(), s.end());

	return s;
}