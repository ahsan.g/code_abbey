
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include "shm_ass_com.h"

int main()
{

  pid_t pid1;
  pid_t pid2;
  pid_t pid3;
  pid_t pid4;

  // Matrix M
  int matrix_M[4][4] = {
    {1, 2, 3, 4},
    {5, 6, 7, 8},
    {4, 3, 2, 1},
    {8, 7, 9, 5}};
    // Matrix N
    int matrix_N[4][4] = {
      {1, 3, 5, 7},
      {2, 4, 6, 8},
      {7, 3, 5, 7},
      {8, 6, 4, 2}};
      // Result and Shared Memory


      printf("Fork program starting\n");
      // First Child
      pid1 = fork();
      // Second child
      pid2 = fork();
      // Third child
      pid3 = fork();
      // Fourth child
      pid4 = fork();

      // Pointer to the struct
      void *shared_memory = (void *)0;
      struct shared_array *shared_array;
      int shmid;

      shmid = shmget((key_t)1234, sizeof(struct shared_array), 0666 | IPC_CREAT);

      if(shmid == -1)
      {
        fprintf(stderr, "%s\n", "shmget failed\n" );
        exit(EXIT_FAILURE);
      }

      shared_memory = shmat(shmid, (void *)0, 0);
      if(shared_memory == (void *)-1)
      {
        fprintf(stderr, "%s\n", "shmat Failed");
        exit(EXIT_FAILURE);
      }
      // Process #1 - This Process will sum the products
      // of row 0 of matrix_M and columns of matrix_N
      if(pid1 == 0 && pid2 > 0 && pid3 > 0 && pid4 > 0)
      {
        // Connect to the shared Memory
        printf("Child Process: working with row %d\n", 0);
        shared_array = (struct shared_array *)shared_memory;
        int sum =0;
        for (int j = 0; j<4; j++){
          for (int i = 0; i<4; i++){
            sum += matrix_M[0][i]*matrix_N[i][j];
          }
          // Print Sum

          shared_array->result[j][0] = sum;
          // Sum back to zero for new colum
          sum =0;
        }
      }
      // Process #2 - This Process will sum the products
      // of row 1 of matrix_M and columns of matrix_N
      if(pid2 == 0 && pid1 > 0 && pid3 > 0 && pid4 > 0)
      {
        printf("Child Process: working with row %d\n", 1);
        // Connect to shared Memory
        shared_array = (struct shared_array *)shared_memory;
        int sum =0;
        for (int j = 0; j<4; j++){
          for (int i = 0; i<4; i++){
            sum += matrix_M[1][i]*matrix_N[i][j];
          }
          //Print Sum
          shared_array->result[j][1] = sum;
          sum =0;
        }
      }
      // Process #3 - This Process will sum the products
      // of row 2 of matrix_M and columns of matrix_N
      if(pid3 == 0 && pid1 > 0 && pid2 > 0 && pid4 > 0)
      {
        printf("Child Process: working with row %d\n", 2);
        //printf("%s\n", "Third child" );
        int result =0;
        shared_array = (struct shared_array *)shared_memory;
        int sum =0;
        for (int j = 0; j<4; j++){
          for (int i = 0; i<4; i++){
            sum += matrix_M[2][i]*matrix_N[i][j];
          }

          shared_array->result[j][2] = sum;
          sum =0;
        }
      }
      // Process #4 - This Process will sum the products
      // of row 3 of matrix_M and columns of matrix_N
      if(pid4 == 0 && pid1 > 0 && pid2> 0 && pid3 > 0)
      {
        printf("Child Process: working with row %d\n",3);
        //printf("%s\n", "Fourth child" );
        shared_array = (struct shared_array *)shared_memory;
        int sum =0;
        for (int j = 0; j<4; j++){
          for (int i = 0; i<4; i++){
            sum += matrix_M[3][i]*matrix_N[i][j];
          }

          shared_array->result[j][3] = sum;
          sum =0;
        }
      }

      if(pid1 > 0 && pid2 > 0 && pid3 > 0 && pid4 > 0)
      {
        int stat_val;
        pid_t child_pid1;
        pid_t child_pid2;
        pid_t child_pid3;
        pid_t child_pid4;

        // Wait for child Process to finish
        child_pid1 = wait(&stat_val);
        child_pid2 = wait(&stat_val);
        child_pid3 = wait(&stat_val);
        child_pid4 = wait(&stat_val);

        printf("%s\n","Parent child" );
        printf("Memory attached at %X\n", (int)shared_memory );
        shared_array = (struct shared_array *)shared_memory;
        for(int j =0; j<4; j++){
          for(int i = 0; i<4; i++)
          {
            printf("%d\n",shared_array->result[i][j]);
          }
        }
      }
    }
