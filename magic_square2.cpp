#include <algorithm>
#include <climits>
#include <iostream>
#include <vector>
using namespace std;

// horizontal flip
vector <vector <int>> reflection(vector <vector <int>> ref)
{
	for (int i = 0; i < 3; i++)
	{
		swap(ref[i][0], ref[i][2]);
	}
	return ref;
}

// rotate = transpose + vertical flip
vector <vector <int>> rotate(vector <vector <int>> ref)
{
	vector <vector <int>> result(3, vector <int>(3));
	//transpose
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			result[i][j] = ref[j][i];
		}
	}

	//vertical flip
	for (int i = 0; i < 3; i++)
	{
		swap(result[0][i], result[2][i]);
	}

	return result;
}

int calc(vector <vector <int>> square, vector <vector <int>> magic)
{
	int cost = 0;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cost += abs(square[i][j] - magic[i][j]);
		}
	}

	return cost;
}

int main()
{
	vector <vector <int>> square(3, vector <int>(3));
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			int temp;
			cin >> temp;
			square[i][j] = temp;
		}
	}

	vector <vector <int>> magic(3, vector <int>(3));
	magic = { {8,1,6},{3,5,7},{4,9,2} };
	int min = INT_MAX;
	int cost = 0;
	int n = 4;

	while (n-- > 0)
	{
		cost = calc(square, magic);
		if (min > cost)
		{
			min = cost;
		}
		magic = rotate(magic);
	}

	magic = reflection(magic);
	n = 4;
	while (n-- > 0)
	{
		cost = calc(square, magic);
		if (min > cost)
		{
			min = cost;
		}
		magic = rotate(magic);
	}

	cout << min << endl;
	return 0;
}
